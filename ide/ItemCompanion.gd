tool
extends Node

# NOT USED ANYMORE

#func _ready():
#	get_parent().connect('script_changed', self, 'on_changed')
#
#func on_changed():
#	print('changed!')


func update_sprite(of_item):
	var prefix = 'undefined'
	if of_item.is_thing:
		prefix = of_item.thing_name
	elif of_item.is_operator:
		prefix = of_item.operator_name
	elif of_item.is_quality:
		prefix = of_item.quality_name
	
	var texture_filename = "res://sprites/items/%s" % prefix
	if of_item.is_text:
		texture_filename += "-text"
	texture_filename += ".png"
	var sprite = of_item.find_node('Sprite')
	sprite.texture = load(texture_filename)
	sprite.offset = Vector2(0, 0)
