tool
extends EditorPlugin

# Godot 3.1
# Godot 3.2 fails and I have not investigated why

# Appears in the top menu of the 2D Editor (press F1)
var container = CONTAINER_CANVAS_EDITOR_MENU

var item_wizard_control
var refresh_button

func _enter_tree():
	add_custom_type("HexagonalTileMap", "TileMap", preload("node/HexagonalTileMap.gd"), preload("icon/hexagon.svg"))
#	add_nodetilemap_ui() # abbr. `ntm`
	item_wizard_control = preload('res://addons/godot-plugin-hexagonal-tilemap/ide/ItemWizard.tscn').instance()
	add_control_to_container(container, item_wizard_control)
	
	refresh_button = item_wizard_control.find_node("Refresh")
	refresh_button.connect('pressed', self, 'refresh_pressed')

func _exit_tree():
#	remove_nodetilemap_ui()
	remove_custom_type("HexagonalTileMap")
	remove_control_from_container(container, item_wizard_control)


################################################################################
## ITEM WIZARD #################################################################

const Level = preload("res://addons/godot-plugin-is-you/entity/Level.gd")

func refresh_pressed():
	print('[ItemWizard] \\ô/`')
	var level = get_editor_interface().get_edited_scene_root()
	if not (level is Level):
		printerr(
			"You may only use this button on Level scenes.\n"+
			"We will hide the button contextually, eventually.\n"+
			"It's a good first contribution ;)"
		)
		return
	
	var amount_snapped = 0
	for item in level.get_all_items():
		item.infer_position()
		amount_snapped += 1
	if 0 < amount_snapped:
		print("[ItemWizard] Snapped %d Items." % amount_snapped)
	
	print("[ItemWizard] Passing turn but only with some of the rules.")
	level.spend_turn(true)

	var amout_updated = 0
	for item in level.get_all_items():
		item.update_sprite()
		amout_updated += 1
	if 0 < amout_updated:
		print("[ItemWizard] Updated %d Item sprites." % amout_updated)
		

## NODE TILE MAP #############################################################
## Use the TileMap editor to create new subscene nodes aligned with the tiling
## Very handy.

#var ntm_helper_control
#var is_ntm_helper_showing = false
#var invoke_button

#func add_nodetilemap_ui():
#	ntm_helper_control = preload("control/node_tile_map_helper.tscn").instance()
#	#ntm_helper_control.connect('tree_entered', self, 'custom_ready')
#
#	var selection_ei = get_editor_interface().get_selection()
#	selection_ei.connect("selection_changed", self, "refresh_ntm_helper")
#
#	invoke_button = ntm_helper_control.find_node("Invoke")
#	invoke_button.connect('pressed', self, 'on_invoke_pressed')
#
#	refresh_ntm_helper()


#func remove_nodetilemap_ui():
#	if ntm_helper_control:
#		ntm_helper_control.free()
#	#ntm_helper_control.queue_free()
#
#
#func refresh_ntm_helper():
#	if is_a_node_tile_map_selected():
#		show_ntm_helper()
#	else:
#		hide_ntm_helper()
#
#
#func show_ntm_helper():
#	if is_ntm_helper_showing:
#		return
#	is_ntm_helper_showing = true
#
#	add_control_to_container(CONTAINER_CANVAS_EDITOR_MENU, ntm_helper_control)
#
#
#func hide_ntm_helper():
#	if not is_ntm_helper_showing:
#		return
#	is_ntm_helper_showing = false
#
#	remove_control_from_container(CONTAINER_CANVAS_EDITOR_MENU, ntm_helper_control)
#
#
#const NodeTileMap = preload("model/NodeTileMap.gd")
#
#func is_a_node_tile_map_selected():
#	var node = get_single_selected_node()
#
#	return node is NodeTileMap
#
#
#func get_single_selected_node():
#	var nodes = get_editor_interface().get_selection().get_selected_nodes()
#	var count = nodes.size()
#	if 0 == count:
#		return null
#	elif 1 < count:
#		return null
#
#	return nodes.front()
#
#
#func on_invoke_pressed():
#	print('[NTM] Preparing to invoke new nodes as needed…')
#	assert is_a_node_tile_map_selected()
#	var node = get_single_selected_node()
#	assert node.scene_to_instantiate
#	assert node.nodes_container_name
#	var container_name = self.name
#	if node.nodes_container_name:
#		container_name = node.nodes_container_name
#	var root_of_possible_containers = node.get_parent()
#	var nodes_container = root_of_possible_containers.find_node(container_name)
#	if not nodes_container:
#		printerr("""
#Cannot find a sibling node whose name matches `%s`.
#Looked at the children of `%s` in the following tree:
#"""  % [container_name, root_of_possible_containers.name])
#		print_tree_pretty()
#		return
#
#	var cells_positions = node.get_used_cells()
#	for cell_position in cells_positions:
#		var tile_set_id = node.get_cellv(cell_position)
#		print_debug("[NTM] Processing cell #%d at %s." % [
#			tile_set_id,
#			cell_position,
#		])
#
#		var scene_to_instantiate = node.scene_to_instantiate
#		var instantiated_scene = scene_to_instantiate.instance()
#		var node_name = "%s(%d,%d)" % [
#			instantiated_scene.name,
#			cell_position.x,
#			cell_position.y,
#		]
#
#		if not nodes_container.find_node(node_name):
#			print_debug("[NTM] Creating `%s`…" % node_name)
#			instantiated_scene.set_name(node_name)
#			instantiated_scene.set_position(
#				node.map_to_world(cell_position)
#				+ node.cell_size * 0.5  # offset to center
#			)
#			nodes_container.add_child(instantiated_scene)
#			instantiated_scene.set_owner(get_tree().get_edited_scene_root())
#		else:
#			instantiated_scene.free()
#			print_debug("[NTM] Found existing node `%s` ‑ skipping…" % node_name)
		
		
