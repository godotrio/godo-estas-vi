
# This is BAD
# - Tied to nodes having hexagon_position as attribute
# - Offset-mode coords ;_⋅

# …
# Perhaps extend the Lattice2D

# Vector2 cell (dumb offset-mode coords) => Array of Nodes

var map = Dictionary()

func reset_map():
	map.clear()

func get_used_cells():
	return self.map.keys()

func find_on_cell(cell):
	return find_at(cell)

func find_at(hexagon_position):
	return self.map.get(hexagon_position, Array())
	# or
#		var found = Array()
#		if map.has(hexagon_position):
#			found = map.get(hexagon_position)
#		return found

func add_node(node):
	var where = node.hexagon_position  # /!.  node is Item
	if not self.map.has(where):
		self.map[where] = Array()
	self.map[where].append(node)

func reindex_from_nodes(nodes):
	reset_map()
	for node in nodes:
		add_node(node)






