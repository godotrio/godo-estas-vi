tool
extends Node2D

# Items are the main (only?) entities in the game.
# 
# It handles:
# - its flags, that the level will reset when spending turn.
# - its appearance, from its flags
# 
# Make sure there are images `<thing>.png` and `<thing>-text.png`
# in `sprites/items`.
# 
# More properties (for animated textures, directional textures, …)
# may be added later through Resources.  (TBD)
# 

# This is relevant only for things.
# Always set this to true for operators and qualities.
# Use "word" or "text' ?  Can't decide...
#export(bool) var is_word = false
export(bool) var is_text = false

# Describe the concepot of this item.
# Only one of these may be set.
# If somehow multiple are provided, behavior is undefined.
# Right now any keyword can be used
# as long as there are icons for it.
# - sprites/items/<name>.png
# - sprites/items/<name>-text.png
# Please only use a single word, lowercase, no diacritics.
export(String) var thing_name
export(String) var operator_name
export(String) var quality_name
# In the future we may use setget to update parts of the Editor,
# and also set is_text, and make sure only one is set, etc.
#export(String) var thing_name setget set_thing_name, get_thing_name
#export(String) var operator_name setget set_operator_name, get_operator_name
#export(String) var quality_name setget set_quality_name, get_quality_name

# Emitted when one of the above properties change,
#signal concept_changed

# Read-only concept flags.
# In this class, use the fully qualified self.is_xxx
# instead of the shortcut is_xxx to trigger setgets.
var is_thing setget set_is_thing, get_is_thing
var is_operator setget set_is_operator, get_is_operator
var is_quality setget set_is_quality, get_is_quality

# This is exported for the ItemWizard.
# It derives it from the node's xy position.
# The game uses this property,
# as the level's lattice reindexes itself from it.
export(Vector2) var hexagon_position = Vector2(0, 0)

# State | Qualities flags.
# These are exported mostly for the Item Wizard.
# They will be reset by the sentences on each turn.
export(bool) var is_you = false
export(bool) var is_win = false
export(bool) var is_stop = false
export(bool) var is_push = false
export(bool) var is_lose = false
export(bool) var is_sink = false
export(bool) var is_open = false
export(bool) var is_shut = false
export(bool) var is_weak = false
export(bool) var is_hot = false
export(bool) var is_melt = false

export(bool) var is_lit = true setget set_lit

func reset_properties():
	self.is_you = false
	self.is_win = false
	self.is_stop = false
	self.is_push = false
	self.is_lose = false
	self.is_sink = false
	self.is_open = false
	self.is_shut = false
	self.is_weak = false
	self.is_hot = false
	self.is_melt = false
	
	if self.is_text:
		self.is_lit = false
	else:
		self.is_lit = true

func to_pickle():
	return {
		'is_you': self.is_you,
		'is_win': self.is_win,
		'is_stop': self.is_stop,
		'is_push': self.is_push,
		'is_lose': self.is_lose,
		'is_sink': self.is_sink,
		'is_open': self.is_open,
		'is_shut': self.is_shut,
		'is_weak': self.is_weak,
		'is_hot': self.is_hot,
		'is_melt': self.is_melt,
		
		'is_lit': self.is_lit,
		
		'is_text': self.is_text,
		'thing_name': self.thing_name,
		'operator_name': self.operator_name,
		'quality_name': self.quality_name,
		'hexagon_position': self.hexagon_position,
	}

func from_pickle(rick):
	for property in rick.keys():
		#prints("[%s] set"%name, property, rick[property])
		self.set(property, rick[property])

################################################################################
# DUPLICATE ####################################################################
# See Level.gd
const DIRECTION_RIGHT = 0
const DIRECTION_UP_RIGHT = 1
const DIRECTION_UP_LEFT = 2
const DIRECTION_LEFT = 3
const DIRECTION_DOWN_LEFT = 4
const DIRECTION_DOWN_RIGHT = 5

const DIRECTION_UP = 777	# You will slalom
const DIRECTION_DOWN = 666	# and will slide.
################################################################################

func get_hex_map():
	return $'../../HexMap'
	
func get_sprite():
	return $Sprite

func get_level():
	return get_parent().get_parent()

################################################################################

func _ready():
	ready()

func ready():
	infer_position()
	update_sprite()
	update_particles()
	rename()

################################################################################


################################################################################

func show_read_only_notice(concept_name, concept_example, value):
	printerr(
		"Don't set is_%s to %s. Consider it read-only.\n" % [
			concept_name, value
		] +
		"Instead, set %s_name to a String, like '%s'." % [
			concept_name, concept_example
		]
	)

func set_is_thing(value):
	show_read_only_notice('thing', 'godo', value)

func get_is_thing():
	return (null != self.thing_name) and not self.thing_name.empty()

func set_is_operator(value):
	show_read_only_notice('operator', 'is', value)

func get_is_operator():
	return (null != operator_name) and not operator_name.empty()

func set_is_quality(value):
	show_read_only_notice('quality', 'you', value)

func get_is_quality():
	return (null != quality_name) and not quality_name.empty()

func set_lit(lit):
	is_lit = lit
	update_salience(lit)

#func set_thing_name(value):
#	if not value:
#		return
#	thing_name = value
#	operator_name = ''
#	quality_name = ''
#	rename()
#	emit_signal("concept_changed", 'thing', value)
#
#func get_thing_name():
#	return thing_name
#
#func set_operator_name(value):
#	if not value:
#		return
#	thing_name = ''
#	operator_name = value
#	quality_name = ''
#	rename()
#	emit_signal("concept_changed", 'operator', value)
#
#func get_operator_name():
#	return operator_name
#
#func set_quality_name(value):
#	if not value:
#		return
#	thing_name = ''
#	operator_name = ''
#	quality_name = value
#	rename()
#	emit_signal("concept_changed", 'quality', value)
#
#func get_quality_name():
#	return quality_name


func infer_position():
	snap_to_grid()
	reposition()

################################################################################

func qualify(quality_name):
	var n = "is_%s" % quality_name
	if n in self:
		set(n, true)

func transmute(thing_name):
	self.thing_name = thing_name
	
	update_sprite()
	rename()

func rename():
	self.name = get_concept_name().capitalize() + ('Word' if self.is_text else '')

func get_concept_name():
	var concept = 'undefined'  # here be trolls
	if self.is_thing:
		concept = thing_name
	elif self.is_operator:
		concept = operator_name
	elif self.is_quality:
		concept = quality_name
	return concept

func update_aesthetics():
	update_sprite()
	update_salience()
	update_particles()

func update_sprite():
	var prefix = get_concept_name()
	var texture_filename = "res://sprites/items/%s" % prefix
	if self.is_text:
		texture_filename += "-text"
	texture_filename += ".png"
	# FIXME: check first and counsel
	get_sprite().texture = load(texture_filename)
	get_sprite().offset = Vector2(0, 0)

func update_particles():
	$IsWinCPUParticles2D.emitting = self.is_win

func update_salience(is_increase=null):
	if null == is_increase:
		is_increase = is_lit
	if is_increase:
		self.modulate = Color(1.0, 1.0, 1.0, 1.0)
	else:
		var darker = 0.62
		self.modulate = Color(darker, darker, darker, 0.82)

func reposition():
	position = get_hex_map().map_to_world(hexagon_position)

func snap_to_grid():
	hexagon_position = get_hex_map().world_to_map(position)
	reposition()

#func destroy():
#	queue_free()

################################################################################
# Monkey code to refactor outta here

func get_items_on(that_hexagon_position):
	var items = get_all_items()
	var found_items = Array()
	for item in items:
		if item.hexagon_position == that_hexagon_position:
			found_items.append(item)
	return found_items

func get_all_items():
	return $'..'.get_children() # :(|)

################################################################################

#func _silence():  # of the debugger
#	return is_win or is_you or is_push or is_stop

################################################################################

var oddr_directions = [
	[[+1,  0], [ 0, -1], [-1, -1], 
	[-1,  0], [-1, +1], [ 0, +1]],
	[[+1,  0], [+1, -1], [ 0, -1], 
	[-1,  0], [ 0, +1], [+1, +1]],
]

func offset_coord(hex, direction):
	var parity = int(hex.y) & 1
	var dir = oddr_directions[parity][direction]
	return Vector2(hex.x + dir[0], hex.y + dir[1])

################################################################################

func can_go_on(that_hexagon_position):
	var map = get_hex_map()
	var is_emptiness = map.get_cellv(that_hexagon_position) == map.INVALID_CELL
	# … add more emptiness conditions here as-needed
	return not is_emptiness

func try_move(direction, dry=false):
	# Shenanigans to support UP and DOWN
	var directions = Array()
	if direction == DIRECTION_UP:
		directions = [DIRECTION_UP_LEFT, DIRECTION_UP_RIGHT]
		if int(self.hexagon_position.y) & 1:
			directions = [DIRECTION_UP_RIGHT, DIRECTION_UP_LEFT]
	if direction == DIRECTION_DOWN:
		directions = [DIRECTION_DOWN_LEFT, DIRECTION_DOWN_RIGHT]
		if int(self.hexagon_position.y) & 1:
			directions = [DIRECTION_DOWN_RIGHT, DIRECTION_DOWN_LEFT]
	if not directions.empty():
		var moved = false
		for one_direction in directions:
			moved = try_move(one_direction, dry)
			if moved:
				break
		return moved
	
	# Ok, let's look at what we have on the target tile
	var next_position = offset_coord(hexagon_position, direction)
	var next_items = get_items_on(next_position)
	
	if not can_go_on(next_position):
		return false
	
	for next_item in next_items:
		if next_item.is_stop and not next_item.is_push:
			return false
	
	var found_items_to_push = false
	for next_item in next_items:
		if next_item.is_push or next_item.is_text:
			found_items_to_push = true
			var can_next_move = next_item.try_move(direction, true)
			if not can_next_move:
				return false
	
	# Apply move on the items on the next tile
	if found_items_to_push and not dry:
		for next_item in next_items:
			if next_item.is_push or next_item.is_text:
				var can_next_move = next_item.try_move(direction, dry)
				if not can_next_move:
					return false
	
	for next_item in next_items:
		if not next_item.is_push and next_item.is_shut and not self.is_open:
			return false
	
	# Aply move to this item
	if not dry:
		hexagon_position = next_position
		reposition()
		
	return true
