tool
extends "res://addons/godot-plugin-is-you/entity/Item.gd"

export(String, FILE, "*.tscn") var destination_level_path

export(Texture) var texture
export(Texture) var texture_when_done


#func ready():
#	.ready()

func infer_position():
	.infer_position()

func rename():
	pass

func update_sprite():
	var sprite = get_sprite()
	if Engine.editor_hint:
		sprite.texture = texture
		return
	if Game.is_level_complete(destination_level_path):
		sprite.texture = texture_when_done
	else:
		sprite.texture = texture

func _input(event):
	if (
		(event is InputEventKey or event is InputEventJoypadButton)
		and event.pressed
		and Input.is_action_just_pressed("ui_accept")
		and is_piled_with_you()
	):
		print("Opening Entry %s." % self.name)
		Game.switch_to_level(destination_level_path)

func is_piled_with_you():
	var it_is = false
	for item in get_all_items():
		if is_piled_with(item) and item.is_you:
			it_is = true
	return it_is

func is_piled_with(other_item):
	return self.hexagon_position == other_item.hexagon_position
