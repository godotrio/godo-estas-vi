tool
extends Node2D

# A Level handles:
# - a bunch of items
# - a hexagonal tileset
# - applying the game rules
# - win and defeat animations
# - its own history (UNDO IS ALL)
# - … ?

export(String, FILE, "*.tscn") var map_scene_path
#export var map_scene_tile := Vector2.ZERO

const Item = preload("res://addons/godot-plugin-is-you/entity/Item.gd")
const HexagonalLattice = preload('res://addons/godot-plugin-hexagonal-tilemap/core/HexagonalLattice.gd')

onready var items_layer = $Items

func _input(event):
	if event is InputEventJoypadButton or event is InputEventKey:
		if Input.is_action_just_released("restart"):
			Game.reset_level()
		if Input.is_action_just_released("pass"):
			spend_turn()
		if Input.is_action_just_released("escape"):
			go_back_to_map()

var hexagon_lattice

const DIRECTION_RIGHT = 0
const DIRECTION_UP_RIGHT = 1
const DIRECTION_UP_LEFT = 2
const DIRECTION_LEFT = 3
const DIRECTION_DOWN_LEFT = 4
const DIRECTION_DOWN_RIGHT = 5
const DIRECTION_UP = 777
const DIRECTION_DOWN = 666


const FIRST_WORD = 0
const SECOND_WORD = 1
const THIRD_WORD = 2


func _ready():
	# We could perhaps instead do this in the _init?
	hexagon_lattice = HexagonalLattice.new()
	
	# This initial turn would handle the initial
	# setting of item flags from the rules.
	# But we don't want this.  Hence the ItemWizard.
	#spend_turn()
	
	# But we want to write the initial state in history,
	# so we can get back to it by pressing undo.
	write_history()
	
	

################################################################################

#func _unhandled_key_input(_event):
#	handle_arrow_inputs()

#func handle_arrow_inputs():
#	var has_moved = false
#
#	if (not has_moved) \
#	and Input.is_action_just_pressed("ui_left"):
#		if Input.is_action_pressed("ui_up"):
#			try_move(DIRECTION_UP_LEFT)
#		elif Input.is_action_pressed("ui_down"):
#			try_move(DIRECTION_DOWN_LEFT)
#		else:
#			try_move(DIRECTION_LEFT)
#		has_moved = true
#
#	if (not has_moved) \
#	and Input.is_action_just_pressed("ui_right"):
#		if Input.is_action_pressed("ui_up"):
#			try_move(DIRECTION_UP_RIGHT)
#		elif Input.is_action_pressed("ui_down"):
#			try_move(DIRECTION_DOWN_RIGHT)
#		else:
#			try_move(DIRECTION_RIGHT)
#		has_moved = true
#
#	if (not has_moved) \
#		and Input.is_action_just_pressed("ui_up"):
#		for item in get_items_that_are_you():
#			var ap = item.get_node('AnimationPlayer')
#			if ap:
#				ap.play('MoveUpConfusion')
#
#	if (not has_moved) \
#		and Input.is_action_just_pressed("ui_down"):
#		for item in get_items_that_are_you():
#			var ap = item.get_node('AnimationPlayer')
#			if ap:
#				ap.play('MoveDownConfusion')
#
#	if has_moved:
#		spend_turn()

################################################################################

func _process(_delta):
	handle_inputs()

export(float) var action_cooldown = 0.16  # s
export(float) var actions_combo_window = 0.16  # s
var last_action_stamp = 0.0  # ms

func handle_inputs():
	var t = OS.get_ticks_msec()
	if t < last_action_stamp + (action_cooldown * 1000):
		return
	
	var has_moved = false
	if (
		(not has_moved)
		and Input.is_action_pressed("move_up")
		and Input.is_action_pressed("move_right")
		):
		has_moved = try_move(DIRECTION_UP_RIGHT)
	if (
		(not has_moved)
		and Input.is_action_pressed("move_up")
		and Input.is_action_pressed("move_left")
		):
		has_moved = try_move(DIRECTION_UP_LEFT)
	if (
		(not has_moved)
		and Input.is_action_pressed("move_down")
		and Input.is_action_pressed("move_right")
		):
		has_moved = try_move(DIRECTION_DOWN_RIGHT)
	if (
		(not has_moved)
		and Input.is_action_pressed("move_down")
		and Input.is_action_pressed("move_left")
		):
		has_moved = try_move(DIRECTION_DOWN_LEFT)
	if (not has_moved) and Input.is_action_pressed("move_right"):
		has_moved = try_move_unless_belayed(DIRECTION_RIGHT)
	if (not has_moved) and Input.is_action_pressed("move_up_right"):
		has_moved = try_move(DIRECTION_UP_RIGHT)
	if (not has_moved) and Input.is_action_pressed("move_up"):
		has_moved = try_move_unless_belayed(DIRECTION_UP)
	if (not has_moved) and Input.is_action_pressed("move_up_left"):
		has_moved = try_move(DIRECTION_UP_LEFT)
	if (not has_moved) and Input.is_action_pressed("move_left"):
		has_moved = try_move_unless_belayed(DIRECTION_LEFT)
	if (not has_moved) and Input.is_action_pressed("move_down_left"):
		has_moved = try_move(DIRECTION_DOWN_LEFT)
	if (not has_moved) and Input.is_action_pressed("move_down"):
		has_moved = try_move_unless_belayed(DIRECTION_DOWN)
	if (not has_moved) and Input.is_action_pressed("move_down_right"):
		has_moved = try_move(DIRECTION_DOWN_RIGHT)
	
	var has_moved_belayable = false
	if (not has_moved):
		has_moved = run_belayable_move_perhaps()
		has_moved_belayable = has_moved
	
	if has_moved:
		last_action_stamp = t % (1 << 30)
		spend_turn()
	
	var has_undoed = false
	if (not has_moved) and Input.is_action_pressed("undo"):
		has_undoed = true
		undo()
	
	if has_moved or has_undoed:  # has_acted
		last_action_stamp = t % (1 << 30)
	
	if has_moved_belayable:
		last_action_stamp -= actions_combo_window * 1000
		

func try_move(direction):
	__belayable_move_direction = null
	var any_has_moved = false
	for item in get_items_that_are_you():
		var that_one_moved = item.try_move(direction)
		if that_one_moved:
			any_has_moved = true
	return any_has_moved


var __belayable_move_direction = null
var __belayable_move_stamp = 0.0  # ms
func try_move_unless_belayed(direction):
	if __belayable_move_direction == direction:
		return false
	__belayable_move_direction = direction
	__belayable_move_stamp = OS.get_ticks_msec()
	return true


func run_belayable_move_perhaps():
	if null == __belayable_move_direction:
		return false
	var stamp = OS.get_ticks_msec()
	if (stamp - __belayable_move_stamp) > self.actions_combo_window * 1000:
		var has_moved = try_move(__belayable_move_direction)
		last_action_stamp = __belayable_move_stamp
		__belayable_move_direction = null  # should we let try_move handle this?
		return has_moved
	return false


func reindex_lattice():
	hexagon_lattice.reindex_from_nodes(get_all_items())


func animate_victory():
#	fill_the_screen()
	$AnimationPlayer.play("Victory", -1, 0.5)


var is_in_limbo = false

func animate_limbo():
	$AnimationPlayer.play("Limbo", -1, 0.5)
	self.modulate = Color(0.9, 0.9, 0.9, 1.0)
	is_in_limbo = true

func exit_limbo():
	if is_in_limbo:
		$AnimationPlayer.play("Limbo", -1, -1)
		self.modulate = Color(1.0, 1.0, 1.0, 1.0)
		is_in_limbo = false


func fill_the_screen():
	# This is only working when it wants to.
	$ScreenFillCPUParticles2D1.emitting = true
	$ScreenFillCPUParticles2D2.emitting = true
	$ScreenFillCPUParticles2D3.emitting = true
	$ScreenFillCPUParticles2D4.emitting = true

################################################################################

func reset_qualities_and_stuff():
	for item in get_all_items():
		item.reset_properties()

#func compute_turn(): who cares how it is named?
func spend_turn(in_editor=false):
	
	# For faster computation, we want to index the items.
	# Here's a good place to reindex, since items may have moved.
	reindex_lattice()
	
	# Yay, let's reset qualities and stuff!
	reset_qualities_and_stuff()
	
	var valid_sentences = Array()
	
	# Find all the possible sentences, even the ones that don't make sense
	var sentences = get_possible_sentences()
	if not sentences.empty():
		print("Found sentences (including absurd)")
		for sentence in sentences:
			print("	%s	%s	%s" % [
				sentence[FIRST_WORD].get_concept_name().to_upper(),
				sentence[SECOND_WORD].get_concept_name().to_upper(),
				sentence[THIRD_WORD].get_concept_name().to_upper(),
			])
	else:
		print("No sentences found at all.")
	
	# I. Alchemy Sentences : THING IS THING
	# I.a Find them all
	var tit_sentences = Array()
	for sentence in sentences:
		if  sentence[FIRST_WORD].is_thing \
		and sentence[SECOND_WORD].is_operator \
		and sentence[SECOND_WORD].operator_name == 'is' \
		and sentence[THIRD_WORD].is_thing:
			tit_sentences.append(sentence)
			valid_sentences.append(sentence)
	
	# I.b Show them in the logs
	if not tit_sentences.empty():
		print("THING IS THING : Alchemical Sentences")
		for sentence in tit_sentences:
			print("	%s	%s	%s" % [
				sentence[FIRST_WORD].get_concept_name().to_upper(),
				sentence[SECOND_WORD].get_concept_name().to_upper(),
				sentence[THIRD_WORD].get_concept_name().to_upper(),
			])
	
	# I.c Apply them (if relevant)
	if in_editor:
		if not tit_sentences.empty():
			print(
				"Skipping application of above alchemical sentences.\n " +
				"Use the SPEND TURN button (todo) to apply them too."
			)
	else:
		for sentence in tit_sentences:
			var to_transmute = get_things(sentence[FIRST_WORD].thing_name, false)
			for item in to_transmute:
				item.transmute(sentence[THIRD_WORD].thing_name)
	
	# II. Qualifying Sentences : THING IS QUALITY
	# II.a Find them all
	var tiq_sentences = Array()
	for sentence in sentences:
		if  sentence[FIRST_WORD].is_thing \
		and sentence[SECOND_WORD].is_operator \
		and sentence[SECOND_WORD].operator_name == 'is' \
		and sentence[THIRD_WORD].is_quality:
			tiq_sentences.append(sentence)
			valid_sentences.append(sentence)
	
	# II.b Print them to the log
	if not tiq_sentences.empty():
		print("THING IS QUALITY : Qualifying Sentences")
		for sentence in tiq_sentences:
			print("	%s	%s	%s" % [
				sentence[FIRST_WORD].get_concept_name().to_upper(),
				sentence[SECOND_WORD].get_concept_name().to_upper(),
				sentence[THIRD_WORD].get_concept_name().to_upper(),
			])
	
	# II.c Apply QUALITY = <any>
	for sentence in tiq_sentences:
		var to_qualify = get_things(sentence[FIRST_WORD].thing_name, false)
		for item in to_qualify:
			item.qualify(sentence[THIRD_WORD].quality_name)
	
	# more special sentences
	# …
	
	# VII. Light up the words of the valid sentences
	for sentence in valid_sentences:
		for word in sentence:
			word.is_lit = true
	
	# VIII. Same-tile effects
	var has_won = false
	if not in_editor:
		apply_weak_effect()
		apply_sink_effect()
		apply_hot_effect()
		has_won = apply_win_effect()
		if not has_won:
			apply_lose_effect()
			apply_open_effect()
	
	# more special cases
	# …
	
	# XX. Defeat|Limbo Scenario : THERE IS NO YOU
	if not in_editor and not has_won:
		var no_you = true
		for item in get_all_items():
			if item.is_you:
				no_you = false
				break
		if no_you:
			animate_limbo()
	
	# Reposition all the items?
	
	# Update the items aesthetics from their new flags
	for item in get_all_items():
#		item.update_aesthetics()
		item.update_particles()
	
	# L. Reregister all the items in case we created some.
	# This could be optimized, if necessary, by registering only when needed.
	for item in get_all_items():
		register_item(item)	
	
	# LI. Write this turn into the history ledger
	write_history()


################################################################################

# All Items (including the destroyed ones) are indexed here.
# We use this to fetch past Items during undo for example.
# This will also keep the past Items safe from garbage collection,
# once they are removed from the scene tree.
var items_bag = Dictionary()  # instance_id => Item

# The history of turns spent on this level.
# Undo truncates the last entry.
# We could use this upon victory to show a replay, that would be awesome.
var history = Array()  # of Dictionary(instance_id => item_pickle)

func hash_item(item):
	return item.get_instance_id()

func register_item(item):
	assert(item is Item)
	var key = hash_item(item)
	if key in self.items_bag:
		assert(item == self.items_bag[key])
		return
	self.items_bag[key] = item

func write_history():
	var this_turn = Dictionary()
	for item in get_all_items():
		this_turn[hash_item(item)] = item.to_pickle()
	history.append(this_turn)

func undo():
	if self.history.size() < 2:
		return
	call_deferred('_deferred_undo')

func _deferred_undo():
	for item in get_all_items():
		self.items_layer.remove_child(item)
	var previous_turn = self.history[self.history.size() - 2]
	for item_hash in previous_turn.keys():
		assert(item_hash in self.items_bag)
		var item = self.items_bag[item_hash]
		item.from_pickle(previous_turn[item_hash])
		self.items_layer.add_child(item)
		item.reposition()
		item.update_aesthetics()
	
	self.history.pop_back()
	exit_limbo()


################################################################################

func get_things(thing_name=null, is_text=null):
	var things = Array()
	for item in get_all_items():
		if item.is_thing:
			if null != thing_name and item.thing_name != thing_name:
				continue
			if null != is_text and item.is_text != is_text:
				continue
			things.append(item)
	return things

func get_all_items():
	var items = Array()
	for item in self.items_layer.get_children():
		if item is Item:
			items.append(item)
	return items

func get_items_with_qualities(qualities_filter):
	var items = Array()
	for item in get_all_items():
		var validates_filter = true
		for quality in qualities_filter.keys():
			var expected_value = qualities_filter[quality]
			if item.get(quality) != expected_value:
				validates_filter = false
				break
		if validates_filter:
			items.append(item)
	return items

func get_text_items():
	# Comprehensions needed
	var text_items = Array()
	for item in get_all_items():
		if item.is_text:
			text_items.append(item)
	return text_items

func get_items_that_are_you():
	# Comprehensions needed
	var you_items = Array()
	for item in get_all_items():
		if item.is_you:
			you_items.append(item)
	return you_items

func get_items_piled_with(item, including_item=false):
	var all_items = self.hexagon_lattice.find_on_cell(item.hexagon_position)
	if not including_item:
		all_items.remove(all_items.find(item))
	return all_items

func get_things_piled_with(item, including_item=false):
	var piled_things = Array()
	if including_item and item.is_thing:
		piled_things.append(item)
	var piled_items = get_items_piled_with(item, false)
	for piled_item in piled_items:
		if item.is_thing and not item.is_text:
			piled_things.append(piled_item)
	return piled_things

################################################################################

func get_possible_sentences():
	var possible_sentences = Array()
	var items = get_text_items()
	for item in items:
		var sentences = get_possible_sentences_from(item.hexagon_position)
		possible_sentences = possible_sentences + sentences
	return possible_sentences

func get_possible_sentences_from(hexagon_position):
	return get_possible_sentences_in_direction(hexagon_position, 0) \
			+ get_possible_sentences_in_direction(hexagon_position, 5)

func get_possible_sentences_in_direction(hexagon_position, direction):
	var sentences = Array()
	var second_position = find_adjacent_position(hexagon_position, direction)
	
	for first_item in hexagon_lattice.find_at(hexagon_position):
		if not first_item.is_text:
			continue
		for second_item in hexagon_lattice.find_at(second_position):
			if not second_item.is_text:
				continue
			if not second_item.is_operator:
				continue
			var third_position = find_adjacent_position(second_item.hexagon_position, direction)
			for third_item in hexagon_lattice.find_at(third_position):
				if not third_item.is_text:
					continue
				sentences.append([first_item, second_item, third_item])
	
	return sentences


################################################################################

func apply_win_effect():
	var has_won = false
	var cells = self.hexagon_lattice.get_used_cells()
	for cell in cells:
		var items_piled = self.hexagon_lattice.find_on_cell(cell)
		var found_win = false
		var found_you = false
		for item in items_piled:
			if item.is_win:
				found_win = true
			if item.is_you:
				found_you = true
		if found_win and found_you:
			has_won = true
			continue
	
	if has_won:
		print("GRATULO !  %s IS WIN" % self.name.to_upper())
		register_victory()
		# The animation will end by a call to go_back_to_map()
		animate_victory()
	
	return has_won

func apply_lose_effect():
	var applied = false
	for item in get_items_with_qualities({'is_lose': true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_you:
				destroy_item(other_item)
				applied = true
	return applied

func apply_sink_effect():
	for item in get_items_with_qualities({'is_sink': true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			destroy_item(other_item)
			destroy_item(item)
			break

func apply_weak_effect():
	for item in get_items_with_qualities({'is_weak': true}):
		var piled_items = get_items_piled_with(item)
		if not piled_items.empty():
			destroy_item(item)

func apply_open_effect():
	var applied = false
	for item in get_items_with_qualities({'is_open': true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_shut:
				destroy_item(other_item)
				destroy_item(item)
				applied = true
				break
	return applied

func apply_hot_effect():
	var applied = false
	for item in get_items_with_qualities({'is_hot': true}):
		var piled_items = get_items_piled_with(item)
		for other_item in piled_items:
			if other_item.is_melt:
				destroy_item(other_item)
				applied = true
	return applied

################################################################################

func destroy_item(item):
	items_layer.remove_child(item)
	reindex_lattice()  # expensive!

################################################################################

func register_victory():
	Game.register_victory()

func go_back_to_map():
	Game.switch_to_level(self.map_scene_path)
#	Game.switch_to_level(self.map_scene_path, {
#		'start_on_tile': map_scene_tile,  # ?
#	})

################################################################################

# Move elsewhere  (HexagonalLattice probably, perhaps the HexagonalTileSet?)
# Code coming from marvelous reblobgames.

# Shameful patching of offset coords instead of using cube.
var oddr_directions = [
	[[+1,  0], [ 0, -1], [-1, -1], 
	[-1,  0], [-1, +1], [ 0, +1]],
	[[+1,  0], [+1, -1], [ 0, -1], 
	[-1,  0], [ 0, +1], [+1, +1]],
]

func find_adjacent_position(hexagon_position, direction):
	var parity = int(hexagon_position.y) & 1
	var dir = oddr_directions[parity][direction]
	return Vector2(hexagon_position.x + dir[0], hexagon_position.y + dir[1])



