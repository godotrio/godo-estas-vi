extends Node

# A Global Singleton for Game Logic

var root
var current_scene = null
var current_scene_path = null

func _ready():
	load_game()
	root = get_tree().get_root()
	self.current_scene = root.get_child(root.get_child_count() - 1)


func reset_level():
	if not current_scene_path:
		printerr(
			"Cannot reset current level " +
			" `switch_to_level(…)` was never called."
		)
		return
	self.switch_to_level(current_scene_path)


func switch_to_level(level_path):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_switch_to_level", level_path)


func _deferred_switch_to_level(level_path):
	# It is now safe to remove the current scene
	current_scene.free()

	# Load the new scene.
	var level_scene = ResourceLoader.load(level_path)

	# Instance the new scene.
	current_scene = level_scene.instance()
	current_scene_path = level_path

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)


func register_victory():
	if not current_save_data.has('levels'):
		current_save_data['levels'] = Dictionary()
	current_save_data['levels'][current_scene_path] = {
		'complete': true,
		#'best_time': ?
	}
	save_game()


var current_save_data = Dictionary()

var save_path = "user://savegame_01.save"

func save_game():
	var save_data_json = to_json(self.current_save_data)
	var save_game = File.new()
	save_game.open(self.save_path, File.WRITE)
	save_game.store_line(save_data_json)
	save_game.close()


func load_game():
	var save_game = File.new()
	if not save_game.file_exists(self.save_path):
		print("[Game] No save to load at `%s'." % self.save_path)
		return  # We don't have a save to load.

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
#	var save_nodes = get_tree().get_nodes_in_group("Persist")
#	for i in save_nodes:
#	    i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(self.save_path, File.READ)
	self.current_save_data = parse_json(save_game.get_line())
	save_game.close()


func is_level_complete(level_path):
	return (
		self.current_save_data.has('levels')
		and self.current_save_data['levels'].has(level_path)
		and self.current_save_data['levels'][level_path]['complete']
	)
