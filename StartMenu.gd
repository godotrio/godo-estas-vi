extends Node2D

export(String, FILE, "*.tscn") var map_scene_path

func _ready():
	$PlayButton.connect("pressed", self, "on_play_pressed")
	$AboutButton.connect("pressed", self, "on_about_pressed")
	$PlayButton.grab_focus()

func on_play_pressed():
	Game.switch_to_level(self.map_scene_path)

func on_about_pressed():
	var url = "https://framagit.org/godotrio/godo-estas-vi#godo-estas-vi"
	OS.shell_open(url)
	OS.set_clipboard(url)
