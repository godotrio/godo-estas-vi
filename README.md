# GODO ESTAS VI

![A screenshot of a puzzle of GODO ESTAS VI](assets/screenshot_01.png)

- BABA IS YOU
- Hexagons
- Esperanto
- YOU, WIN, STOP, PUSH, LOSE, SINK, OPEN, SHUT, WEAK, HOT, MELT
- Complete level editor

Basically, the product of a three day game jam between friends.


## Controls

    NUMPAD IS MOVE
    ARROW IS MOVE
    ENTER IS ENTER
    ESCAPE IS ESCAPE
    SPACE IS PASS
    R IS RESTART
    U IS UNDO


## Tentative TODO

You're welcome to hack any of these, as well as to add levels or art.

- [ ] TELE
- [ ] MOVE
- [ ] FLOAT
- [ ] TEXT
- [ ] AND
- [ ] HAS
- [ ] …
- [ ] More levels
- [ ] Music
- [ ] Settings
- [ ] Multiple save slots
- [ ] Mouse controls (A* included)


## GOD IS YOU

_You make the game._

1. Download [Godot 3.1](https://godotengine.org/download/),
2. Download [the sources of GODO ESTAS VI](https://framagit.org/godotrio/godo-estas-vi/-/tags).
3. Run Godot.
4. Open this project.
5. ???
6. Enjoy!

> Remember to request a merge of your additions, so that everyone can enjoy them!


### Making Levels

The current levels in `levels/intro` are badly designed, as you probably noticed.  We were interested in the code, and we only made levels to test the logic.  You can trash them all and make new ones.

> Video tutorial : https://youtu.be/MWfkT296RGQ

Go to `Scene > New inherited scene`, choose `core/Level.tscn`.

You can duplicate the Item nodes, change their concepts,
move them around, and hit REFRESH in the top dock of the 2D Editor.

Save your scene somewhere in `levels/`.

---

You can also make a new map, or submaps.


### Making art

_Yes, please._

The directory `sprites/items` is particularly in need, since [kenney](https://www.kenney.nl/)'s art is mighty fine.
But don't feel limited in any way ; open a discussion in the issues to showcase what you envision, and we'll make it happen.


## Credits

- Goutte
- Raf
- Docus
- Sylvain
- Natha
- Adèle
- [Kenney](https://www.kenney.nl/) for their awesome CC0 art

