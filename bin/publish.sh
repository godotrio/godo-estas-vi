#!/usr/bin/env bash

GAME_NAME="godo-estas-vi"
#VERSION=`bash bin/get_version.sh`
#HTML5="${GAME_NAME}_${VERSION}_html5"

REMOTE="antoine.qrok.me"
REMOTE_ROOT="/home/goutenoir/antoine/games"
REMOTE_PATH="${REMOTE_ROOT}/${GAME_NAME}"

echo -e "Publishing ${GAME_NAME} ${VERSION}\n"
echo -e "on ${REMOTE} at ${REMOTE_PATH}"

## RSYNC HTML5 WITH SERVER
rsync --verbose \
    --recursive \
    build/html5/* ${REMOTE}:${REMOTE_PATH}

## RSYNC BINARIES WITH SERVER
#rsync -vr \
#    --exclude="build/html5" \
#    build/ ${REMOTE}:${REMOTE_PATH}/downloads

echo -e "Published!"
